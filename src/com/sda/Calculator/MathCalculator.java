package com.sda.Calculator;

public class MathCalculator {

    public static double pow(double what, int toPower) {
        double result = 1;
        for (int i = 0; i < toPower; i++) {
            result *= what;
        }
        return result;
    }
}
