package com.sda.math;

public class MyMath {
    public static int abs(int a) {
        if (a >= 0) {
            return a;
        } else {
            return -a;
        }
    }

    public static double abs(double a) {
        if (a >= 0) {
            return a;
        } else {
            return -a;
        }
    }

    // a = 5
    // wynik = 5
    // a = -5
    // wynik = 5

    public static int pow(int a, int b) {
        // a - jaka liczba
        // b - do której potęgi ma być podniesiona liczba a
        int wynik = 1;
        for (int i = 0; i < b; i++) {
            wynik *= a;
        }
        return wynik;
    }

    public static double pow(double a, int b) {
        // a - jaka liczba
        // b - do której potęgi ma być podniesiona liczba a
        double wynik = 1;
        for (int i = 0; i < b; i++) {
            wynik *= a;
        }
        return wynik;
    }
}
