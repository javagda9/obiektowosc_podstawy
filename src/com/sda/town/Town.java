package com.sda.town;

public class Town {
    // tablica obywateli
    private Citizen[] citizens;

    public Town() {
    }

    public Citizen[] getCitizens() {
        return citizens;
    }

    public void setCitizens(Citizen[] citizens) {
        this.citizens = citizens;
    }

    public int howManyCanVote() {
        int howManyCanVote = 0; // licznik
        // petla przez tablice obywateli
        for (Citizen citizen : citizens) {
            // sprawdzenie czy obywatel moze glosowac
            if (citizen.canVote()) {
                // jesli moze to zwiekszam licznik
                howManyCanVote++;
            }
        }
        // na koncu zwracam ilos osob ktore moga glosowac.
        return howManyCanVote;
    }

    public String[] whoCanVote() {
        int ileMozeGlosowac = howManyCanVote();

        String[] names = new String[ileMozeGlosowac];

        int indeks = 0;
        for (Citizen citizen : citizens) {
            if (citizen.canVote()) {
                names[indeks] = citizen.getName();
                indeks++;
            }
        }

        return names;
    }
}
