package com.sda.town;

public class King extends Citizen{

    public King(String name) {
        super(name);
    }

    public void introduce(){
        System.out.println(name);
    }
}
