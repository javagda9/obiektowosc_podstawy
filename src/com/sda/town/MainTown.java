package com.sda.town;

public class MainTown {
    public static void main(String[] args) {
        Town t = new Town();
        Citizen[] citizens = new Citizen[]{
                new King("Marian"),
                new Soldier("Marek"),
                new Soldier("Jarek"),
                new Soldier("Darek"),
                new Soldier("Arek"),
                new Peasant("Bob"),
                new Peasant("Michał"),
                new Peasant("Waldek"),
                new Townsman("Max"),
        };
        t.setCitizens(citizens);

        System.out.println(t.howManyCanVote());
        String[] names = t.whoCanVote();
        for (String name : names) {
            System.out.println(name);
        }
    }
}
