package com.sda.town;

public abstract class Citizen {
    protected String name;

    public Citizen(String name) {
        this.name = name;
    }

//    public abstract boolean canVote();
    public boolean canVote(){
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
