package com.sda.student;

import java.util.Random;

public class Student {

    private int[] oceny;

    public Student() {
        generate();
    }

    private void generate() {
        Random r = new Random();
        oceny = new int[10];
        for (int i = 0; i < 10; i++) {
            oceny[i] = r.nextInt(5) + 1;
        }
    }
}
