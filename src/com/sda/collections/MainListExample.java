package com.sda.collections;

import java.util.ArrayList;
import java.util.Arrays;

public class MainListExample {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5, 6};
        tab[0] = 1;
        System.out.println(tab);
        System.out.println(tab.length);
        System.out.println(tab[3]);
        tab[4] = 0;

        // 1 linia ponizej zadeklarowanie listy typu string z inicjalnymi wartosciami
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("a", "b", "c", "d", null, "g", "g", null));
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1); // dodanie na koncu listy elementu 1
        list.add(2);
        list.add(3);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(0, 10); // dodanie na indeksie 0 elementu 10

        System.out.println(list); // wypisanie listy
        System.out.println(list.size()); // wypisanie rozmiaru listy
        System.out.println(list.get(3)); // wypisanie z listy elementu na indeksie 3

        System.out.println(list);
        list.remove(1); // usuniecie z listy elementu na indeksie 1
        System.out.println(list);
        list.remove(1);
        System.out.println(list);
        list.remove(1);
        list.remove(null); // usuniecie z listy elementu null (pierwsze wystapienie)

        System.out.println(list);


    }
}
