package com.sda.samochod;

public class Kabriolet extends Samochod {
    private boolean dachSchowawny;

    public void schowajDach() {
        dachSchowawny = true;
    }

    public boolean czyDachSchowany() {
        return dachSchowawny;
    }
}
