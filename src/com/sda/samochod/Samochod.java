package com.sda.samochod;

public class Samochod {
    private double predkosc;
    private boolean lightsOn;

    public void przyspiesz() {
        if (predkosc <= 110) {
            predkosc += 10;
        } else {
            predkosc = 120;
        }
    }

    public void wlaczSwiatla() {
        lightsOn = true;
    }

    public boolean czySwiatlaWlaczone() {
        return lightsOn;
    }

    public double getPredkosc() {
        return predkosc;
    }

    public void setPredkosc(double predkosc) {
        this.predkosc = predkosc;
    }


    public void setLightsOn(boolean lightsOn) {
        this.lightsOn = lightsOn;
    }
}
