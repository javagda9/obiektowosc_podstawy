package com.sda.samochod;

public class MainKabrio {
    public static void main(String[] args) {
        Samochod kb = new Kabriolet();
        if (kb instanceof Kabriolet) {
            Kabriolet kabriolet = (Kabriolet) kb;
            System.out.println("Dach: " + kabriolet.czyDachSchowany());
        }
    }
}
