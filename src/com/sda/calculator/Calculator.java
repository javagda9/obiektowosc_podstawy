package com.sda.calculator;

public class Calculator {
    public double add(double a, double b) {
        return a + b;
    }

    public double sub(double a, double b) {
        return a - b;
    }

    public double mul(double a, double b) {
        return a * b;
    }

    public double div(double a, int b) {
        try {
            double wynik = a / b;
            return wynik;
        } catch (ArithmeticException ae) {
            System.err.println("Dzielenie przez zero!");
            return 0;
        }
    }
}
