package com.sda.calculator;

import java.util.Scanner;

public class MainCalculator {
    public static void main(String[] args) {

//        System.out.println("Podaj A:");
//        int a = sc.nextInt();
//        System.out.println("Podaj B:");
//        int b = sc.nextInt();
//        System.out.println(calculator.div(a,b));
//
//
//        System.out.println("Podaj A:");
//        int a = sc.nextInt();
//        System.out.println("Podaj B:");
//        int b = sc.nextInt();
//        System.out.println(calculator.add(a,b));

        // dodaj a b
        // odejmij a b
        // pomnoz a b
        // podziel a b
        // Dodaj

        // '     dodaj a b      '
        // 'dodaj a b'
        Calculator calculator = new Calculator();
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine();
            line = line.toLowerCase().trim();

            String[] word = line.split(" ");

            String komenda;
            String liczbaA;
            String liczbaB;
            try {
                komenda = word[0];

                liczbaA = word[1];
                liczbaB = word[2];
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                System.err.println("Niezrozumiała komenda, napisz: dodaj/odejmij/pomnoz/podziel");
                continue;
            }

            double a;
            int b;
            // a i b nie są zainicjalizowane. Musimy zadeklarować je wcześniej
            // ponieważ jeśli zadeklarujemy je w try_catch, to z uwagi na czas zycia
            // zmiennej, przestaną istnieć po wyjściu z tego bloku.
            try {
                a = Double.parseDouble(liczbaA);
                b = Integer.parseInt(liczbaB);
            } catch (NumberFormatException nfe) {
                System.err.println("Blad parsowania, niepoprawna wartosc.");
                System.err.println(nfe.getMessage());
                // continue zapobiegnie wykonaniu dalszych instrukcji w tej petli
                // spowoduje pominiecie dalszej czesci kodu i powrot na poczatek petli

                continue;
            }

            if ("dodaj".equals(komenda)) {
                System.out.println(calculator.add(a, b));
            } else if ("odejmij".equals(komenda)) {
                System.out.println(calculator.sub(a, b));
            } else if ("pomnoz".equals(komenda)) {
                System.out.println(calculator.mul(a, b));
            } else if ("podziel".equals(komenda)) {
                System.out.println(calculator.div(a, b));
            }

        }
    }
}
