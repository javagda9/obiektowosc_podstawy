package com.sda.familymembers;

public class Daughter extends FamilyMember {

    @Override
    public boolean isAdult() {
        return false;
    }

    @Override
    public void introduce() {
        System.out.println("I am daughter.");
    }
}
