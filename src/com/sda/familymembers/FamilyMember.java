package com.sda.familymembers;

public abstract class FamilyMember {

    public abstract boolean isAdult();

    public void introduce(){
        System.out.println("I am just a simple family member...");
    }

}
