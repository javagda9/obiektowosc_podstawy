package com.sda.familymembers;

public class Son extends FamilyMember{

    @Override
    public boolean isAdult() {
        return false;
    }

    @Override
    public void introduce() {
        System.out.println("I am son.");
    }
}
