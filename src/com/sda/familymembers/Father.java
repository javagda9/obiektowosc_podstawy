package com.sda.familymembers;

public class Father extends FamilyMember{

    @Override
    public boolean isAdult() {
        return true;
    }

    @Override
    public void introduce() {
        System.out.println("I am father.");
    }
}
