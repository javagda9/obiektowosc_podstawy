package com.sda.familymembers;

public class Mother extends FamilyMember {

    @Override
    public boolean isAdult() {
        return true;
    }

    @Override
    public void introduce() {
        System.out.println("I am mother.");
        super.introduce();
    }

    public void introduce(String name) {
        System.out.println("I am mother, my name is: " + name);
    }

    public void introduce(int age) {
        System.out.println("I am mother, i am " + age + " years old.");
    }
}
