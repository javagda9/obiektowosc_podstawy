package com.sda.ksztaltyDziedziczenie;

public class Prostokat extends Ksztalt {
    public Prostokat(double bokA, double bokB) {
        super(bokA, bokB);
    }

    @Override
    public void wypiszCos() {
        System.out.println("Prostokat o bokach: " + bokA + " " + bokB);
    }

}
