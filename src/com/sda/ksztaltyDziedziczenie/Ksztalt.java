package com.sda.ksztaltyDziedziczenie;

public abstract class Ksztalt implements IKsztalt{
    protected double bokA;
    protected double bokB;

    public Ksztalt(double bokA, double bokB) {
        this.bokA = bokA;
        this.bokB = bokB;
    }

    public double obliczPole() {
        return bokA * bokB;
    }

    public double obliczObwod() {
        return bokA + bokB + bokA + bokB;
    }

    public double getBokA() {
        return bokA;
    }

    public void setBokA(double bokA) {
        this.bokA = bokA;
    }

    public double getBokB() {
        return bokB;
    }

    public void setBokB(double bokB) {
        this.bokB = bokB;
    }
}
