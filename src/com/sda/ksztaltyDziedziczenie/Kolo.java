package com.sda.ksztaltyDziedziczenie;


public class Kolo {
    private double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    public double obliczObwód() {
        return 2 * Math.PI * promien;
    }

    public double obliczPole() {
//        MathCalculator calculator = new MathCalculator();

        return Math.PI * Math.pow(promien, 2);
    }

}
