package com.sda.ksztaltyDziedziczenie;

public class Kwadrat extends Ksztalt {
    public Kwadrat(double bok) {
        super(bok, bok);
    }

    @Override
    public void wypiszCos() {
        System.out.println("To jest kwadrat. Bok:" + getBokA());
    }
// Trattoria2014
    @Override
    public double obliczPole() {
        return getBokA() * getBokA();
    }
}
