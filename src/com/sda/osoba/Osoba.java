package com.sda.osoba;

public class Osoba {
    private int rokUrodzenia;
    private String imie;

    public Osoba(int rokUrodzenia, String imie) {
        this.rokUrodzenia = rokUrodzenia;
        this.imie = imie;
    }

    public void przedstawSie() {
        int wiek = 2018 - rokUrodzenia;
        System.out.println("Cześć! Mam na imie " + imie + " i mam " + wiek + " lat.");
    }

    public String getImie() {
        return imie;
    }

    @Override
    public String toString() {
        return "com.sda.osoba.Osoba{" +
                "rokUrodzenia=" + rokUrodzenia +
                ", imie='" + imie + '\'' +
                '}';
    }
}
