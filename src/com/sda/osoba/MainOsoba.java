package com.sda.osoba;

public class MainOsoba {
    public static void main(String[] args) {
        Osoba os1 = new Osoba(1993, "Ania");
        Osoba os2 = new Osoba(1965, "Andrzej");
        Osoba os3 = new Osoba(1967, "Mariola");

        os1.przedstawSie();
        os2.przedstawSie();
        os3.przedstawSie();

        System.out.println(os1.getImie());

        System.out.println(os1);
        System.out.println(os1.toString());
    }
}
