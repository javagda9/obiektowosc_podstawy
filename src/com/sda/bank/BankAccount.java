package com.sda.bank;

public class BankAccount {
    private long accountNumber;
    private int money;

    public BankAccount(long accountNumber, int money) {
        this.accountNumber = accountNumber;
        this.money = money;
    }

    public void wplacSrodki(int srodki) {
        money += srodki;
    }

    private void nothing(){

    }

    // money = 1000
    // ileWyplacic = 2000
    public int wyplacSrodki(int ileWyplacic) {
        if (money >= ileWyplacic) {
            // mamy opowiednia ilosc pieniedzy
            money -= ileWyplacic;
            return ileWyplacic;
        } else {
            // brak pieniedzy
            System.out.println("Brak wystarczającej ilości środków.");
            int ileWyplace = money;
            money = 0;

            return ileWyplace;
        }
    }

    public void podajStanKonta() {
        System.out.println("Konto o numerze " + accountNumber + " stan: " + money);
    }
}
