package com.sda.bank;

public class MainBank {

    public static void main(String[] args) {
        BankAccount bankAccountAndrzej = new BankAccount(123L, 1000);
        BankAccount bankAccountMariola = new BankAccount(124L, 2000);
        System.out.println(bankAccountAndrzej.wyplacSrodki(10));
        System.out.println(bankAccountAndrzej.wyplacSrodki(10));
        System.out.println(bankAccountAndrzej.wyplacSrodki(10));
        System.out.println(bankAccountAndrzej.wyplacSrodki(10));
        System.out.println(bankAccountAndrzej.wyplacSrodki(10));

        bankAccountMariola.wplacSrodki(1000);
        bankAccountMariola.wyplacSrodki(5000);

        bankAccountAndrzej.wplacSrodki(50);
        bankAccountAndrzej.podajStanKonta();

        bankAccountMariola.podajStanKonta();
    }
}
