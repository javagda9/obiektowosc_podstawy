package com.sda.ksztalty;

public class Kwadrat {
    private double bok;

    public Kwadrat(double bok) {
        this.bok = bok;
    }

    public Kwadrat() {
    }

    public double obliczObwód() {
        return 4 * bok;
    }

    public double obliczPole() {
        return bok * bok;
    }

    public double getBok() {
        return bok;
    }

    public void setBok(double bok) {
        this.bok = bok;
    }

    @Override
    public String toString() {
        return "Kwadrat{" +
                "bok=" + bok +
                '}';
    }
}
