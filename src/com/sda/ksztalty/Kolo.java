package com.sda.ksztalty;

import com.sda.Calculator.MathCalculator;


public class Kolo {
    private double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    public double obliczObwód() {
        return 2 * Math.PI * promien;
    }

    public double obliczPole() {
//        MathCalculator calculator = new MathCalculator();

        return Math.PI * Math.pow(promien, 2);
    }

}
