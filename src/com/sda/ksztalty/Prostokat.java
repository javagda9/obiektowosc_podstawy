package com.sda.ksztalty;

public class Prostokat {
    private double bokA;
    private double bokB;

    public Prostokat(double bokA, double bokB) {
        this.bokA = bokA;
        this.bokB = bokB;
    }

    public double obliczObwód() {
        return 2 * bokA + 2 * bokB;
    }

    public double obliczPole() {
        return bokA * bokB;
    }

}
