package com.sda.qe;

public class QEMain {
    public static void main(String[] args) {
        try {
            metoda2();
        } catch (DeltaLessThanZeroException dle) {
            System.err.println(dle.getMessage());
        }
    }

    public static void metoda() throws DeltaLessThanZeroException {
        throw new DeltaLessThanZeroException();
//        try {
//            throw new DeltaLessThanZeroException();
//        }catch (DeltaLessThanZeroException dltze){
//            System.out.println("Error");
//        }
    }

    public static void metoda2(){
        metoda();
    }
}
