package com.sda.qe;

// Exception - wyjątek który zglosi blad kompilacji jesli nie jest przechwycony
//public class DeltaLessThanZeroException extends Exception{
//}
public class DeltaLessThanZeroException extends RuntimeException{
    public DeltaLessThanZeroException() {
        super("Your delta is less than zero!");
    }
}