package com.sda.qe;

public class QuadraticEquation {
    private double a, b, c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calcDelta() {
        return b * b - (4 * a * c);
    }

    public double calcX1() {
        double d = calcDelta();
        if (d > 0) {
            return (-b - Math.sqrt(d)) / (2 * a);
        } else if (d == 0) {
            return (-b) / (2 * a);
        }
        throw new DeltaLessThanZeroException();
    }

    public double calcX2() {
        double d = calcDelta();
        if (d > 0) {
            return (-b + Math.sqrt(d)) / (2 * a);
        } else if (d == 0) {
            throw new NoSecondSolutionException();
        }
        throw new DeltaLessThanZeroException();
    }


}
