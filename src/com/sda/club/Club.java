package com.sda.club;

public class Club {
    public void enter(Person osobaKtoraChceWejsc) throws NoAdultException{
        if(osobaKtoraChceWejsc.getAge() < 18){
            throw new NoAdultException();
        }
    }
}
