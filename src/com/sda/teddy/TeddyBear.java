package com.sda.teddy;

public class TeddyBear {
    private String name;

    public TeddyBear(String name) {
        this.name = name;
    }

    public void printName(){
        System.out.println("Imie misia: " + name);
    }

    @Override
    public String toString() {
        return "com.sda.teddy.TeddyBear{" +
                "name='" + name + '\'' +
                '}';
    }
}
