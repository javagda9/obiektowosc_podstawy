public class Car {
    // protected
    // public
    // private
    //
    private String nrRejestracyjny;

    public Car(String nrRejestracyjny) {
        this.nrRejestracyjny = nrRejestracyjny;
    }

    public String getNrRejestracyjny() {
        return nrRejestracyjny;
    }

    public void setNrRejestracyjny(String nrRejestracyjny) {
        this.nrRejestracyjny = nrRejestracyjny;
    }

    public void drive(){
        System.out.println("Car is driving");
    }
}
